var randomstring = require("randomstring");
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/myDB";

MongoClient.connect(url, { useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  var dbo = db.db("myDB");
  dbo.collection("authors").drop();
  dbo.collection("books").drop();
  testDB(dbo);

});



function createAuthor(firstName, lastName, birthYear, booksArray, id) {
  return {
    firstName : firstName,
    lastName : lastName,
    fullname : firstName + " " + lastName,
    birthYear : birthYear,
    booksArray : booksArray,
    id : id
  }
}

function createBook(name, description, publishDate, author, pagesNum) {
  return {
    name : name,
    description : description,
    publishDate : publishDate,
    author : author,
    pagesNum : pagesNum,
  }
}


async function insertAuthor(obj, dbo, collection) {
  try {
    await dbo.collection(collection).insertOne(obj);
    // console.log("1 author inserted");
  } catch {err => {
    console.error(err)
    }}
}

async function testDB(dbo) {

  try {
    await dbo.createCollection("authors");
    await dbo.createCollection("books");
    console.log("Collection created!");

    var allBooksArray = [];
    for(bookNum = 0; bookNum < 1000; bookNum++) {
      var createYear = getRandomInt(1990, 2025);
      var pagesNum = getRandomInt(150, 250);
      var currBook = createBook(randomstring.generate(), randomstring.generate(), createYear, "author", pagesNum );
      allBooksArray = allBooksArray.concat([currBook]);
      dbo.collection("books").insertOne(currBook);

    }
    for(authID = 0; authID < 100; authID++) {
      var birthYear = getRandomInt(1990, 2025);
      var booksArray = allBooksArray.slice(authID, (authID+1)*10-1);
      var currAuth = createAuthor(randomstring.generate(), randomstring.generate(), birthYear, booksArray, authID);
      await insertAuthor(currAuth, dbo, "authors");
    }
    console.log("Collection inserted!");

    var authorCollection = dbo.collection("authors");
    await authorCollection.createIndex({ id : 1 })

    // await dbo.collection("authors").createIndex({ "booksArray.description": 1})
    // await dbo.collection("authors").createIndex({ "booksArray.name": 1})

    await authorCollection.createIndex({ "booksArray.pagesNum": 1 })

    const output = await authorCollection.aggregate([
      { $unwind: '$booksArray' },
      { $match: { "booksArray.pagesNum": {$gt: 200},
      "booksArray.publishDate": {$gte : 2015 , $lte : 2020},
      "firstName": /^P/i}},
      { $project: {"fullname" : 1, "booksArray.name" : 1}}
   ]).toArray();
   console.log('hello')
   console.log(output)

  } catch {err => {
    console.error(err)
    }}

}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}